<?php

namespace Drupal\extension_sanitization\EventSubscriber;

use Drupal\Core\File\Event\FileUploadSanitizeNameEvent;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Filename extension sanitization event subscriber.
 */
class ExtensionSanitizationSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Sanitize a filename during upload.
   *
   * @param \Drupal\Core\File\Event\FileUploadSanitizeNameEvent $event
   *   The file upload event.
   */
  public function sanitizeFilename(FileUploadSanitizeNameEvent $event) {
    $allowed_extensions = $event->getAllowedExtensions();
    $filename = $event->getFilename();
    $exploded_filename = explode('.', $filename);
    $changed = FALSE;
    // Go from the second last element to the second.
    for ($i = count($exploded_filename) - 2; $i >= 1; $i -= 1) {
      if (in_array(strtolower($exploded_filename[$i]), $allowed_extensions)) {
        unset($exploded_filename[$i]);
        $changed = TRUE;
      }
    }
    if ($changed) {
      $event->setFilename(implode('.', $exploded_filename));
      $this->messenger->addMessage('File was renamed due to multiple file extensions.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FileUploadSanitizeNameEvent::class => 'sanitizeFilename',
    ];
  }

}
